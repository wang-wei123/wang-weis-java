package com.control;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import com.entity.User;
/**
 * 类说明
 *登录控制
 * @author 陈毅隆
 * @date 2021-1-25 新建
 */
public class LoginControl {
	public LoginControl(){
		
	}
	
	/**注册
	*0:注册成功 1:用户已存在 
	*/
	public int register(String user, String pass) {
		List<User> myusers = readtxt();
		
		for(int i=0;i<myusers.size();i++) {
			if(myusers.get(i).getUsername().equals(user)) {
				return 1;
			}
		}
		
		writetxt(user+" "+pass);
		return 0;
	}
	/**登录
	 * 0:登录成功 2:用户不存在 3:密码错误
	*/ 
	public int login(String user, String pass) {
		List<User> myusers = readtxt();
		
		for(int i=0;i<myusers.size();i++) {
			if(myusers.get(i).getUsername().equals(user)) {
				if(myusers.get(i).getPassword().equals(pass)){
					return 0;
				}
				else {
					return 3;
				}
			}
		}
		
		return 2;
	}
	
	
	/**读取文件
	 */
	public List<User> readtxt() {
		List<User> myusers = new ArrayList<User>();
		try {
            String encoding="GBK";
            File file=new File("D:\\USER.txt");
            /**判断文件是否存在
            * 
            */
            if(file.isFile() && file.exists()){ 
            	/**GBK考虑到编码格式
            	 */
                InputStreamReader read = new InputStreamReader(new FileInputStream(file),encoding);
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTxt = null;
                while((lineTxt = bufferedReader.readLine()) != null){
                	String[] mess = lineTxt.split(" ");
                	if(mess.length == 2) {
                		 User t = new User(mess[0],mess[1]);
                         myusers.add(t);
                	}
                }
                read.close();
            }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		return myusers;
	}
	/**写入文件
	 */
	public void writetxt(String s) {
		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("D:\\USER.txt", true)));
			out.write(s+"\n");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
