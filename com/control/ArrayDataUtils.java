package com.control;

import java.util.ArrayList;
import com.entity.Student;
/**
 * 类说明
 *转成Object[][]数组
 * @author 陈毅隆
 * @date 2021-1-25 新建
 */

public class ArrayDataUtils {
	public static Object[][] toCustomerArray(ArrayList<Student> data) {
		Object[][] o = new Object[data.size()][8];
		for (int i = 0; i < data.size(); i++) {
			Student cust = (Student) data.get(i);
			o[i][0] = cust.getId();
			o[i][1] = cust.getName();
			o[i][2] = cust.getSex();
			o[i][3] = cust.getBirthday();
			o[i][4] = cust.getStatus();
			o[i][5] = cust.getAddress();
			o[i][6] = cust.getPhone();
			o[i][7] = cust.getRoomid();
		}
		return o;
	}
}
