package com.control;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import com.entity.Student;
/**
 * 类说明
 *学生操作控制
 * @author 陈毅隆
 * @date 2021-1-25 新建
 */
public class StudentControl {
	/**0:添加成功 -1：信息已存在 1:宿舍已满
	 */
	static final int NUMBER = 4;
	
	public int add(Student stu) {
		Student exist = findbyid(stu.getId());
		if (exist != null) {
			return -1;
		}
		if (checkroomid(stu.getRoomid()) == false) {
			return 1;
		}
		String s = stu.message();
		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("D:\\STUDENT.txt", true)));
			out.write(s + "\n");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	/** 0: 修改成功 -1:学号不存在 1:宿舍已满
	 */
	public int edit(String eid, String name, String status, String birth, String phone, String address, String room) {
		Student stu = findbyid(eid);
		if (stu == null) {
			return -1;
		}
		if (!room.equals(stu.getRoomid())) {
			if (checkroomid(room) == false) {
				return 1;
			}
		}
		delete(eid);
		stu.setName(name);
		stu.setStatus(status);
		stu.setBirthday(birth);
		stu.setPhone(phone);
		stu.setAddress(address);
		stu.setRoomid(room);
		add(stu);

		return 0;
	}

	public Student findbyid(String id) {
		try {
			String encoding = "GBK";
			File file = new File("D:\\STUDENT.txt");
			//判断文件是否存在
			if (file.isFile() && file.exists()) { 
				//考虑到编码格式
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), encoding);
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					String[] mess = lineTxt.split(" ");
					if (mess.length > 1) {
						if (mess[0].equals(id)) {
							Student t = new Student(mess[0], mess[1], mess[2], mess[3], mess[4], mess[5], mess[6],
									mess[7]);
							return t;
						}
					}
				}
				read.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<Student> findbyroomid(String roomid) {
		ArrayList<Student> stu = new ArrayList<Student>();
		try {
			String encoding = "GBK";
			File file = new File("D:\\STUDENT.txt");
			// 判断文件是否存在
			if (file.isFile() && file.exists()) { 
				// 考虑到编码格式
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), encoding);
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					String[] mess = lineTxt.split(" ");
					if (mess.length > 1) {
						Student t = new Student(mess[0], mess[1], mess[2], mess[3], mess[4], mess[5], mess[6], mess[7]);
						if (t.getRoomid().equals(roomid)) {
							stu.add(t);
						}
					}
				}
				read.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stu;
	}

	public ArrayList<Student> findbyname(String name) {
		ArrayList<Student> stu = new ArrayList<Student>();
		try {
			String encoding = "GBK";
			File file = new File("D:\\STUDENT.txt");
			// 判断文件是否存在
			if (file.isFile() && file.exists()) { 
				// 考虑到编码格式
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), encoding);
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					String[] mess = lineTxt.split(" ");
					if (mess.length > 1) {
						Student t = new Student(mess[0], mess[1], mess[2], mess[3], mess[4], mess[5], mess[6], mess[7]);
						if (t.getName().equals(name)) {
							stu.add(t);
						}
					}
				}
				read.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stu;
	}

	public int delete(String id) {
		ArrayList<Student> stu = new ArrayList<Student>();
		try {
			String encoding = "GBK";
			File file = new File("D:\\STUDENT.txt");
			// 判断文件是否存在
			if (file.isFile() && file.exists()) { 
				// 考虑到编码格式
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), encoding);
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				int flag = -1;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					String[] mess = lineTxt.split(" ");
					if (mess.length > 1) {
						Student t = new Student(mess[0], mess[1], mess[2], mess[3], mess[4], mess[5], mess[6], mess[7]);
						if (!t.getId().equals(id)) {
							stu.add(t);
						} else {
							flag = 0;
						}
					}
				}
				read.close();
				if (flag == -1) {
					return -1;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("D:\\STUDENT.txt", false)));
			for (int i = 0; i < stu.size(); i++) {
				String s = stu.get(i).message();
				out.write(s + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	/**true:宿舍可继续添加 false:宿舍不可继续添加
	 */
	public boolean checkroomid(String roomid) {
		int num = 0;

		try {
			String encoding = "GBK";
			File file = new File("D:\\STUDENT.txt");
			// 判断文件是否存在
			if (file.isFile() && file.exists()) { 
				// 考虑到编码格式
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), encoding);
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					String[] mess = lineTxt.split(" ");
					if (mess.length > 1) {
						Student t = new Student(mess[0], mess[1], mess[2], mess[3], mess[4], mess[5], mess[6], mess[7]);
						if (t.getRoomid().equals(roomid)) {
							num++;
						}
					}
				}
				read.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

        
		if (num < NUMBER) {
			return true;
		}
		return false;
	}
}
