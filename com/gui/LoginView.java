package com.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.BevelBorder;
import java.awt.Color;
import javax.swing.border.TitledBorder;

import com.control.LoginControl;

import java.awt.GridLayout;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Button;
import javax.swing.UIManager;
import java.awt.TextField;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * 类说明
 *登录面板
 * @author 王威
 * @date 2021-1-26 新建
 */
public class LoginView extends JFrame {

	private JPanel contentPane;
	static final int NUMBER_2 = 2;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					LoginView frame = new LoginView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginView() {
		setResizable(false);
		setTitle("学生基本信息管理");
		//EXIT_ON_CLOSE（在 JFrame 中定义）：使用 System exit 方法退出应用程序。仅在应用程序中使用。
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 384, 215);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		//背景色
		panel.setBackground(new Color(240, 240, 240));
		//前景色
		panel.setForeground(new Color(255, 0, 0));
		//注册要在工具提示中显示的文本。光标处于该组件上时显示该文本。
		panel.setToolTipText(""); 
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "\u8BF7\u767B\u5F55", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(15, 0, 335, 113);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel userLabel = new JLabel("用户名 : ");
		userLabel.setFont(new Font("黑体", Font.PLAIN, 18));
		userLabel.setBounds(35, 29, 93, 21);
		panel.add(userLabel);
		
		JLabel passwordLabel = new JLabel("密 码 : ");
		passwordLabel.setFont(new Font("黑体", Font.PLAIN, 18));
		passwordLabel.setBounds(47, 65, 81, 21);
		panel.add(passwordLabel);
		
		TextField username = new TextField();
		username.setFont(new Font("Dialog", Font.PLAIN, 20));
		username.setBounds(133, 29, 148, 25);
		panel.add(username);
		
		TextField password = new TextField();
		password.setFont(new Font("Dialog", Font.PLAIN, 20));
		password.setBounds(133, 65, 148, 25);
		panel.add(password);
		
		Button registerButton = new Button("注 册");
		registerButton.setBounds(77, 119, 66, 30);
		contentPane.add(registerButton);
		
		Button loginButton = new Button("登 录");
		loginButton.setBounds(149, 119, 66, 30);
		contentPane.add(loginButton);
		
		Button exitButton = new Button("退 出");
		exitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				JOptionPane.showMessageDialog(null, "欢迎下次使用！","再见", JOptionPane.INFORMATION_MESSAGE); 

			}
		});
		exitButton.setBounds(221, 119, 66, 30);
		contentPane.add(exitButton);
		
		registerButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LoginControl key = new LoginControl();
				String user = username.getText().toString();
				String pass = password.getText().toString();
				if("".equals(user)||"".equals(pass))
				{
					JOptionPane.showMessageDialog(null, "用户名或密码为空", "输入错误", JOptionPane.WARNING_MESSAGE); 
				}
				else {
					int value = key.register(user, pass);
					//注册成功
					if( value == 0) {
						JOptionPane.showMessageDialog(null, "注册成功，请去登录","注册成功", JOptionPane.INFORMATION_MESSAGE); 
					}
					//已经存在
					else if(value == 1) {
						JOptionPane.showMessageDialog(null, "用户已存在", "注册失败", JOptionPane.ERROR_MESSAGE); 

					}
				}	
			}
		});
		
		loginButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LoginControl key = new LoginControl();
				String user = username.getText().toString();
				String pass = password.getText().toString();
				if("".equals(user)||"".equals(pass))
				{
					JOptionPane.showMessageDialog(null, "用户名或密码为空", "登录失败", JOptionPane.WARNING_MESSAGE); 
				}
				else {
					int value = key.login(user, pass);
					//登录成功
					if( value == 0) {
						JOptionPane.showMessageDialog(null, "欢迎使用！","登录成功", JOptionPane.INFORMATION_MESSAGE); 
						dispose();
						MainView m = new MainView();
						m.setVisible(true);
					}
					//登录失败
					else if(value == NUMBER_2) {
						JOptionPane.showMessageDialog(null, "用户不存在", "登录失败", JOptionPane.ERROR_MESSAGE); 

					}
					else {
						JOptionPane.showMessageDialog(null, "密码错误", "登录失败", JOptionPane.ERROR_MESSAGE); 
					}
				}
			}
		});
	}
}
