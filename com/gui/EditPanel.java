package com.gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import com.control.StudentControl;
import com.entity.Student;

import java.awt.Button;
import java.awt.Panel;
import javax.swing.JComboBox;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;
/**
 * 类说明
 *修改学生面板
 * @author 王威
 * @date 2021-1-26 新建
 */
public class EditPanel extends JPanel {
	private JTextField editid;
	private JTextField namet;
	private JTextField birtht;
	private JTextField phonet;
	private JTextField addresst;
	private JTextField roomt;
	private JPanel panel;
	StudentControl stucontrol = new StudentControl();
	/**
	 * Create the panel.
	 */
	public EditPanel() {
		setLayout(null);
		
		JLabel lblNewLabel = new JLabel("修改信息学号");
		lblNewLabel.setBounds(10, 15, 108, 21);
		add(lblNewLabel);
		
		editid = new JTextField();
		editid.setBounds(120, 12, 126, 27);
		add(editid);
		editid.setColumns(10);
		
		Button sureb = new Button("确  定");
		sureb.setBounds(277, 9, 126, 30);
		add(sureb);
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		//设置当前字体，根据指定名称、样式（粗体）和磅值大小，创建一个新 Font。
		panel.setFont(new Font("Dialog", Font.BOLD, 16));
		//设置前景颜色
		panel.setForeground(new Color(0, 0, 0));
		panel.setBounds(15, 45, 401, 129);
		add(panel);
		panel.setLayout(null);
		
		JLabel nameLabel = new JLabel("姓  名");
		nameLabel.setBounds(20,6, 81, 21);
		panel.add(nameLabel);
		
		namet = new JTextField();
		namet.setBounds(106, 6, 96, 27);
		panel.add(namet);
		namet.setColumns(10);
		
		JLabel statusLabel = new JLabel("政治面貌");
		statusLabel.setBounds(207, 6, 81, 21);
		panel.add(statusLabel);
		
		JLabel birLabel = new JLabel("出生日期");
		birLabel.setBounds(20, 37, 81, 21);
		panel.add(birLabel);
		
		birtht = new JTextField();
		birtht.setBounds(106, 34, 96, 27);
		panel.add(birtht);
		birtht.setColumns(10);
		
		JLabel phoneLabel = new JLabel("电  话");
		phoneLabel.setBounds(20, 65, 81, 21);
		panel.add(phoneLabel);
		
		phonet = new JTextField();
		phonet.setBounds(106, 64, 283, 27);
		panel.add(phonet);
		phonet.setColumns(10);
		
		JComboBox statust = new JComboBox();
		statust.setModel(new DefaultComboBoxModel(new String[] {"共青团员","普通群众"}));
		statust.setBounds(293, 6, 96, 27);
		panel.add(statust);
		
		JLabel label = new JLabel("家庭住址");
		label.setBounds(20, 97, 81, 21);
		panel.add(label);
		
		addresst = new JTextField();
		addresst.setColumns(10);
		addresst.setBounds(106, 96, 283, 27);
		panel.add(addresst);
		
		JLabel roomIdLabel = new JLabel("宿舍号");
		roomIdLabel.setBounds(207, 34, 81, 21);
		panel.add(roomIdLabel);
		
		roomt = new JTextField();
		roomt.setColumns(10);
		roomt.setBounds(293, 31, 96, 27);
		panel.add(roomt);
		
		Button exitButton = new Button("修  改");
		//按钮未启动
		exitButton.setEnabled(false);
		exitButton.setBounds(162, 180, 105, 30);
		add(exitButton);

		sureb.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String eid = editid.getText().toString();
				if("".equals(eid)) {
					JOptionPane.showMessageDialog(null, "未输入学号！", "输入错误", JOptionPane.WARNING_MESSAGE); //输入错误是标题，未输入学号是内容
				}
				else {
					Student stu = stucontrol.findbyid(eid);
					if(stu == null) {
						JOptionPane.showMessageDialog(null, "学号:"+eid+"信息不存在！", "修改失败", JOptionPane.ERROR_MESSAGE); 
					}
					else {
						namet.setText(stu.getName());
						statust.setSelectedItem(stu.getStatus());
						birtht.setText(stu.getBirthday());
						phonet.setText(stu.getPhone());
						addresst.setText(stu.getAddress());
						roomt.setText(stu.getRoomid());
						
						exitButton.setEnabled(true);//按钮启动
					}
				}
			}
		});
		
		exitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String eid = editid.getText().toString();
				String name = namet.getText().toString();
				String status = statust.getSelectedItem().toString();
				String birth = birtht.getText().toString();
				String phone = phonet.getText().toString();
				String address = addresst.getText().toString();
				String room = roomt.getText().toString();
				if("".equals(phone) ||"".equals(eid) || "".equals(name) || "".equals(birth)|| "".equals(room)|| "".equals(status)|| "".equals(address)) {
					JOptionPane.showMessageDialog(null, "存在未填写项！", "输入错误", JOptionPane.WARNING_MESSAGE); 
				}
				else {
					int value = stucontrol.edit(eid,name,status,birth,phone,address,room);
					if(value == 0) {
						JOptionPane.showMessageDialog(null, "学号："+eid+"信息已修改！","修改成功", JOptionPane.INFORMATION_MESSAGE); 
						
						editid.setText("");
						namet.setText("");
						birtht.setText("");
						phonet.setText("");
						statust.setSelectedItem("共青团员");
						addresst.setText("");
						roomt.setText("");
						exitButton.setEnabled(false);
					
					}
					else if(value == -1) {
						JOptionPane.showMessageDialog(null, "学号："+eid+"信息不存在！", "修改失败", JOptionPane.ERROR_MESSAGE); 
					}
					else {
						JOptionPane.showMessageDialog(null, "宿舍："+room+"已住满4人！", "修改失败", JOptionPane.ERROR_MESSAGE); 
					}
				}
			}
		});
	}
}
