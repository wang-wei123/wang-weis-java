package com.gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.control.StudentControl;

import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * 类说明
 *删除学生面板
 * @author 王威
 * @date 2021-1-26 新建
 */
public class DeletePanel extends JPanel {
	private JTextField textField;

	/**
	 * Create the panel.
	 */
	public DeletePanel() {
		setLayout(null);
		
		JLabel idLabel = new JLabel("输入要删除的学号：");
		idLabel.setBounds(15, 15, 167, 21);
		add(idLabel);
		
		textField = new JTextField();
		textField.setBounds(180, 12, 167, 27);
		add(textField);
		textField.setColumns(10);
		
		Button deleteButton = new Button("删 除");
		deleteButton.setBounds(102, 74, 155, 30);
		add(deleteButton);

		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String id = textField.getText().toString();
				if("".equals(id)) {
					JOptionPane.showMessageDialog(null, "未输入学号！", "输入错误", JOptionPane.WARNING_MESSAGE); 
				}
				else {
					StudentControl con = new StudentControl();
					int value = con.delete(id);
					if(value == 0) {
						JOptionPane.showMessageDialog(null, "信息已经删除！","删除成功", JOptionPane.INFORMATION_MESSAGE); 
					}
					else {
						JOptionPane.showMessageDialog(null, "信息不存在！", "删除失败", JOptionPane.ERROR_MESSAGE); 
					}
				}
			}
		});
	}

}
