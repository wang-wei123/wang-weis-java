package com.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * 类说明
 *主界面面板
 * @author 王威
 * @date 2021-1-26 新建
 */
public class MainView extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public MainView() {
		setResizable(false);
		setTitle("学生基本信息管理系统");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 309);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 428, 31);
		contentPane.add(menuBar);
		
		JMenu addNewMenu = new JMenu("新增 （a）");
		menuBar.add(addNewMenu);
		
		JMenuItem addNewMenuItem = new JMenuItem("新增学生信息");
		addNewMenu.add(addNewMenuItem);
		
		JMenu editNewMenu = new JMenu("修改 （e）");
		menuBar.add(editNewMenu);
		
		JMenuItem editNewMenuItem = new JMenuItem("修改学生信息");
		editNewMenu.add(editNewMenuItem);
		
		JMenu findNewMenu = new JMenu("查找（f）");
		menuBar.add(findNewMenu);
		
		JMenuItem findidNewMenuItem = new JMenuItem("学号查询学生信息");
		findNewMenu.add(findidNewMenuItem);
		
		JMenuItem findnameNewMenuItem = new JMenuItem("姓名查询学生信息");
		findNewMenu.add(findnameNewMenuItem);
		
		JMenuItem findroomidNewMenuItem = new JMenuItem("宿舍号查询学生信息");
		findNewMenu.add(findroomidNewMenuItem);
		
		JMenu deleteNewMenu = new JMenu("删除（d）");
		menuBar.add(deleteNewMenu);
		
		JMenuItem deleteNewMenuItem = new JMenuItem("删除学生信息");
		deleteNewMenu.add(deleteNewMenuItem);
		
		AddPanel add = new AddPanel();
		add.setVisible(false);
		add.setBounds(3, 35, 428, 227);
		contentPane.add(add);
		
		DeletePanel delete = new DeletePanel();
		delete.setVisible(false);
		delete.setBounds(40, 80, 428, 227);
		contentPane.add(delete);
		
		EditPanel edit = new EditPanel();
		edit.setVisible(false);
		edit.setBounds(0, 27, 428, 227);
		contentPane.add(edit);

		addNewMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				delete.setVisible(false);
				edit.setVisible(false);
				add.setVisible(true);
			}
		});
		deleteNewMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				edit.setVisible(false);
				add.setVisible(false);
				delete.setVisible(true);
			}
		});
		editNewMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				add.setVisible(false);
				delete.setVisible(false);
				edit.setVisible(true);
			}
		});
		
		findidNewMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				add.setVisible(false);
				delete.setVisible(false);
				edit.setVisible(false);
				FindbyidView f = new FindbyidView();
				f.setVisible(true);
			}
		});
		findroomidNewMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				add.setVisible(false);
				delete.setVisible(false);
				edit.setVisible(false);
				FindbyroomidView f = new FindbyroomidView();
				f.setVisible(true);
			}
		});
		
		findnameNewMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				add.setVisible(false);
				delete.setVisible(false);
				edit.setVisible(false);
				FindbynameView f = new FindbynameView();
				f.setVisible(true);
			}
		});
	}
}
