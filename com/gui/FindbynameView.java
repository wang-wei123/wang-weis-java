package com.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import com.control.ArrayDataUtils;
import com.control.StudentControl;
import com.entity.Student;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Button;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
/**
 * 类说明
 *按姓名查找学生面板
 * @author 王威
 * @date 2021-1-26 新建
 */
public class FindbynameView extends JFrame {

	private JPanel contentPane;
	private JTextField findid;
	private JTable table;
	private JScrollPane scrollPane;
	StudentControl con = new StudentControl();
	/**
	 * Create the frame.
	 */
	public FindbynameView() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 833, 344);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("输入查找的姓名：");
		label.setBounds(212, 14, 154, 21);
		contentPane.add(label);
		
		findid = new JTextField();
		findid.setColumns(10);
		findid.setBounds(371, 11, 96, 27);
		contentPane.add(findid);
		
		Button sure = new Button("确 定");
		sure.setBounds(480, 11, 105, 30);
		contentPane.add(sure);
		
		Object[][] row = {};		
		String[] column = {"学号","姓名","性别","出生日期","政治面貌","家庭住址","电话","宿舍号"};
		/**设置模型
		 * 
		 */
		DefaultTableModel model = new DefaultTableModel(row, column);
		table = new JTable(model);
		table.setBounds(10, 15, 521, 171);
		DefaultTableCellRenderer r = new DefaultTableCellRenderer();   
		r.setHorizontalAlignment(JLabel.CENTER);   
		table.setDefaultRenderer(Object.class, r);
		scrollPane = new JScrollPane(table);
		scrollPane.setEnabled(false);
		scrollPane.setBounds(15, 46, 781, 212);
		contentPane.add(scrollPane);
		
		sure.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String id = findid.getText().toString();
				ArrayList<Student> stu = con.findbyname(id);
				if(stu.size()>0) {
					Object[][] row = ArrayDataUtils.toCustomerArray(stu);		
					String[] column = {"学号","姓名","性别","出生日期","政治面貌","家庭住址","电话","宿舍号"};
					DefaultTableModel modelTemp = new DefaultTableModel(row, column);//设置模型
					table.setModel(modelTemp);	
				}
				else {
					JOptionPane.showMessageDialog(null, "该姓名信息不存在！", "查询失败", JOptionPane.ERROR_MESSAGE); 
				}
			}
		});
	}
}
