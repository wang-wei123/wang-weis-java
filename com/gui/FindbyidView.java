package com.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import com.control.ArrayDataUtils;
import com.control.StudentControl;
import com.entity.Student;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Button;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;
import java.awt.Color;
/**
 * 类说明
 *按学号查找学生面板
 * @author 王威
 * @date 2021-1-26 新建
 */
public class FindbyidView extends JFrame {

	private JPanel contentPane;
	private JTextField findid;
	private JTable table;
	/**滚动面板JScrollPane能实现这样的要求，JScrollPane是带有滚动条的面板。
	 * 
	 */
	private JScrollPane scrollPane;
	StudentControl con = new StudentControl();

	/**
	 * Create the frame.
	 */
	public FindbyidView() {
		// 设置是否可以通过某个用户操作调整 JInternalFrame 的大小。
		setResizable(false);
		// 调用任意已注册 WindowListener 的对象后自动隐藏并释放该窗体。
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 833, 344);
		contentPane = new JPanel();
		// 该类提供了一个占用空间但不执行绘制的空透明边框。 创建具有指定 insets 的空边框
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		// 增加面板
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel label = new JLabel("输入查找的学号：");
		label.setBounds(212, 14, 154, 21);
		contentPane.add(label);

		findid = new JTextField();
		findid.setColumns(10);
		findid.setBounds(371, 11, 96, 27);
		contentPane.add(findid);

		Button sure = new Button("确 定");
		sure.setBounds(480, 11, 105, 30);
		contentPane.add(sure);

		Object[][] row = {};
		String[] column = { "学号", "姓名", "性别", "出生日期", "政治面貌", "家庭住址", "电话", "宿舍号" };
		/*
		 * 设置模型，构造一个 DefaultTableModel， 并通过将 row 和 column 传递到 setDataVector 方法来初始化该表。
		 * 显示JTable组件。
		 */
		DefaultTableModel model = new DefaultTableModel(row, column);
		table = new JTable(model);
		//设置是否可以选择此模型中的行。
		table.setRowSelectionAllowed(false);
		table.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(220, 20, 60)));
		table.setBounds(10, 15, 521, 171);
		//呈现（显示） JTable 中每个单元格的标准类。
		DefaultTableCellRenderer r = new DefaultTableCellRenderer();
		//设置标签内容沿 X 轴的对齐方式。 CENTER（只显示图像的标签的默认值）
		r.setHorizontalAlignment(JLabel.CENTER);
		table.setDefaultRenderer(Object.class, r);
		//滚动面板JScrollPane能实现这样的要求，JScrollPane是带有滚动条的面板。
		scrollPane = new JScrollPane(table);
		scrollPane.setEnabled(false);
		scrollPane.setBounds(15, 46, 781, 212);
		contentPane.add(scrollPane);

		sure.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String id = findid.getText().toString();
				Student stu = con.findbyid(id);
				if (stu != null) {
					ArrayList<Student> stuu = new ArrayList<Student>();
					stuu.add(stu);
					Object[][] row = ArrayDataUtils.toCustomerArray(stuu);
					String[] column = { "学号", "姓名", "性别", "出生日期", "政治面貌", "家庭住址", "电话", "宿舍号" };
					// 设置模型
					DefaultTableModel modelTemp = new DefaultTableModel(row, column);
					table.setModel(modelTemp);
				} else {
					JOptionPane.showMessageDialog(null, "该学号信息不存在！", "查询失败", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}
}
