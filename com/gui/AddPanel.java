package com.gui;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.TextField;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Button;
import javax.swing.border.TitledBorder;

import com.control.StudentControl;
import com.entity.Student;

import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * 类说明
 *添加学生面板
 * @author 王威
 * @date 2021-1-26 新建
 */
public class AddPanel extends JPanel {
	/**文本框
	 * 
	 */
	private JTextField idText;
	private JTextField nameText;
	private JTextField birthdaytext;
	private JTextField phoneText;
	private JTextField addressText;
	private JTextField roomidText;
	StudentControl stuControl = new StudentControl();
	/**
	 * Create the panel.
	 */
	public AddPanel() {
		//设置前景颜色
		setForeground(new Color(0, 0, 0));
		//设置JPanel边框，用指定的边框、标题、标题对齐方式（最前面靠顶对齐）、标题位置、标题字体和标题颜色创建 TitledBorder实例
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 0, 0)));
		//布局
		setLayout(null);
		
		//定义标签
		JLabel idNewLabel = new JLabel("学  号");
		//x坐标、y坐标、组件宽度、组件高度
		idNewLabel.setBounds(15, 15, 81, 21);
		add(idNewLabel);
		
		idText = new JTextField();
		idText.setBounds(101, 15, 96, 27);
		add(idText);
		//设置列数
		idText.setColumns(10);
		
		JLabel nameNewLabel = new JLabel("姓  名");
		nameNewLabel.setBounds(212, 15, 81, 21);
		add(nameNewLabel);
		
		nameText = new JTextField();
		nameText.setBounds(298, 15, 106, 27);
		add(nameText);
		nameText.setColumns(10);
		
		JLabel sexNewLabel = new JLabel("性  别");
		sexNewLabel.setBounds(15, 47, 81, 21);
		add(sexNewLabel);
		
		JComboBox sextext = new JComboBox();
		//下拉列表选择
		sextext.setModel(new DefaultComboBoxModel(new String[] {"男", "女"}));
		sextext.setBounds(101, 47, 96, 27);
		add(sextext);
		
		JLabel birNewLabel = new JLabel("出生日期");
		birNewLabel.setBounds(212, 47, 81, 21);
		add(birNewLabel);
		
		birthdaytext = new JTextField();
		birthdaytext.setBounds(298, 47, 106, 27);
		add(birthdaytext);
		birthdaytext.setColumns(10);
		
		JLabel roomidLabel = new JLabel("宿舍号");
		roomidLabel.setBounds(15, 83, 81, 21);
		add(roomidLabel);
		
		JLabel phoneLabel = new JLabel("电  话");
		phoneLabel.setBounds(15, 122, 81, 21);
		add(phoneLabel);
		
		JComboBox statustext = new JComboBox();
		statustext.setModel(new DefaultComboBoxModel(new String[] {"共青团员","普通群众"}));
		statustext.setBounds(298, 80, 106, 27);
		add(statustext);
		
		phoneText = new JTextField();
		phoneText.setColumns(10);
		phoneText.setBounds(101, 119, 303, 27);
		add(phoneText);
		
		JLabel statusLabel = new JLabel("政治面貌");
		statusLabel.setBounds(212, 83, 81, 21);
		add(statusLabel);
		
		JLabel addressLabel = new JLabel("家庭住址");
		addressLabel.setBounds(15, 158, 81, 21);
		add(addressLabel);
		
		addressText = new JTextField();
		addressText.setColumns(10);
		addressText.setBounds(101, 155, 303, 27);
		add(addressText);
		
		roomidText = new JTextField();
		roomidText.setColumns(10);
		roomidText.setBounds(101, 80, 96, 27);
		add(roomidText);
		
		Button sure = new Button("确  认");
		sure.setBounds(171, 189, 105, 30);
		add(sure);

		
		sure.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String id = idText.getText().toString();
				String name = nameText.getText().toString();
				String sex = sextext.getSelectedItem().toString();
				String bir = birthdaytext.getText().toString();
				String room = roomidText.getText().toString();
				String status = statustext.getSelectedItem().toString();
				String address = addressText.getText().toString();
				String phone = phoneText.getText().toString();
				if("".equals(phone) ||"".equals(id) ||"".equals(name) ||"".equals(sex) ||"".equals(bir) ||"".equals(room) ||"".equals(status)||"".equals(address)) {
					JOptionPane.showMessageDialog(null, "存在未填写项！", "输入错误", JOptionPane.WARNING_MESSAGE); 
				}
				else {
					Student stu = new Student(id,name,sex,bir,status,address,phone,room);
					int value = stuControl.add(stu);
					if(value == 0) {
						JOptionPane.showMessageDialog(null, "学号："+id+"信息已添加！","添加成功", JOptionPane.INFORMATION_MESSAGE); 
						idText.setText("");
						nameText.setText("");
						sextext.setSelectedItem("男");
						birthdaytext.setText("");
						roomidText.setText("");
						statustext.setSelectedItem("共青团员");
						addressText.setText("");
						phoneText.setText("");
					}
					else if(value == 1){
						JOptionPane.showMessageDialog(null, "宿舍："+room+"已住满4人！", "添加失败", JOptionPane.ERROR_MESSAGE); 
					}
					else {
						JOptionPane.showMessageDialog(null, "学号："+id+"信息已存在！", "添加失败", JOptionPane.ERROR_MESSAGE); 
					}

				}
			}
		});
	}
}
