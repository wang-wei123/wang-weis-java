package com.entity;
/**
 * 类说明
 *学生信息，学号、姓名、性别、出生日期、政治面貌、家庭住址、电话、宿舍号。
 * @author 陈毅隆
 * @date 2021-1-25 新建
 *
 */
public class Student {
	private String id;
	private String name;
	private String sex;
	private String birthday;
	private String status;
	private String address;
	private String phone;
	private String roomid;
	public Student(String a, String b, String c, String d, String e, String f,
			String g, String h) {
		id = a;
		name= b;
		sex= c;
		birthday= d;
		status= e;
		address= f;
		phone= g;
		roomid= h;
	}
	public Student() {
		// TODO Auto-generated constructor stub
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getRoomid() {
		return roomid;
	}
	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}
	public String message() {
		return id+" "+name+" "+sex+" "+birthday+" "+status+" "+address+" "+phone+" "+roomid;
	}
}
