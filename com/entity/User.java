package com.entity;
/**
 * 类说明
 *用户信息
 * @author 陈毅隆
 * @date 2021-1-25 新建
 */

public class User {
	private String username;
	private String password;
	public User(String a, String b) {
		username = a;
		password = b;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
